package tracelog

import (
	"io/ioutil"
	"os"
	"path/filepath"
	"strconv"
	"testing"
	"time"
)

func TestTrace(t *testing.T) {
	Trace(TLTest, 1, "testing the trace")
	Trace(TLInfo, 1, "testing format for number: %d", 4564566)
	Trace(TLInfo, 1, "testing pointer format: %p", &TLError)
	Trace(TLInfo, 1, "testing bignumber: %v", 3465384658365468.45749865)
	Trace(TLInfo, 1, "testing bignumber: %d", 3465384658365468.45749865)
	Trace(TLInfo, 1, "testing bignumber: %f", 3465384658365468.45749865)
}

func TestVerbosity(t *testing.T) {
	SetVerbosity(5)
	Trace(TLTest, 1, "testing verbosity")
	Trace(TLTest, 6, "this should not print")
}

func TestFileLogging(t *testing.T) {
	Trace(TLTest, 1, "testing file open")
	td, err := ioutil.TempDir("", "logtest")
	if err != nil {
		t.Fatalf("creating a temp file failed because: %s", err.Error())
	}
	defer os.Remove(td)

	filename := filepath.Join(td, "logtest.log")
	ok, err := SetLogFile(filename, 5)
	if !ok {
		t.Fatalf("Set Log File returned not OK")
	}
	if err != nil {
		t.Fatalf("SetLogFile did something silly: %s", err.Error())
	}

	Trace(TLTest, 1, "This should be logged to the log file "+filename+" as well as screen")
	time.Sleep(time.Millisecond * 100)
	fi, err := os.Stat(filename)
	if err != nil {
		t.Fatalf("unable to check the file log size because: %s ", err.Error())
	}
	if 0 == fi.Size() {
		t.Fatal("log file still zero bytes even after logging")
	}
	CloseLogFile()
	if fileLog != nil {
		t.Fatal("failed to close log file")
	}
	Trace(TLTest, 1, "Testing whether a closed log file generates an error")
}

func TestStress(t *testing.T) {
	Trace(TLTest, 1, "testing 100 goroutine trace calls")
	for i := 100; i > 0; i-- {
		go Trace(TLTest, 1, "Hammering the trace log. Call number: "+strconv.Itoa(i))
	}
	// need to give it a chance to process the goroutines
	time.Sleep(time.Millisecond * 100)
}

func TestColours(t *testing.T) {
	Trace(TLTest, 1, "Testing standard colour combos - this is test")
	Trace(TLDebug, 1, "This is a debug message")
	Trace(TLError, 1, "This is an Error")
	Trace(TLInfo, 1, "This is an information message")
	Trace(Reverse+Bright+FgCyan+BgMagenta, 1, "This is painful")
}
