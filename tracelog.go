//package tracelog provides a logging utility that can be configured on the fly to be more or less verbose
//and have multiple outputs, and deal with log entries coming from multiple goroutines without racing
package tracelog

import (
	"fmt"
	"log"
	"os"
)

const (
	Reset      = "\x1b[0m"
	Bright     = "\x1b[1m"
	Dim        = "\x1b[2m"
	Underscore = "\x1b[4m"
	Blink      = "\x1b[5m"
	Reverse    = "\x1b[7m"
	Hidden     = "\x1b[8m"

	FgBlack   = "\x1b[30m"
	FgRed     = "\x1b[31m"
	FgGreen   = "\x1b[32m"
	FgYellow  = "\x1b[33m"
	FgBlue    = "\x1b[34m"
	FgMagenta = "\x1b[35m"
	FgCyan    = "\x1b[36m"
	FgWhite   = "\x1b[37m"

	BgBlack   = "\x1b[40m"
	BgRed     = "\x1b[41m"
	BgGreen   = "\x1b[42m"
	BgYellow  = "\x1b[43m"
	BgBlue    = "\x1b[44m"
	BgMagenta = "\x1b[45m"
	BgCyan    = "\x1b[46m"
	BgWhite   = "\x1b[47m"
)

// Constants for standard output

var TLError = BgBlue + FgRed + Blink
var TLInfo = BgBlack + FgGreen
var TLDebug = BgBlack + FgYellow
var TLTest = BgBlack + FgBlue

var fileLog *log.Logger
var logVerbosity int = 5  //default middle
var fileVerbosity int = 3 // default low

var logQ chan string
var fileQ chan string

func init() {
	logQ = make(chan string, 100)
	go watchLog(logQ)
	fileQ = make(chan string, 100)
	go watchFile(fileQ)
}

//Trace is the function that adds a message to the log
// codes determines what appearance codes should be used for stdOut
// priority determines whether this message is actually output (lower priority is better)
// message is the actual message to log
func Trace(codes string, priority int, format string, values ...interface{}) {
	if priority < logVerbosity {
		m := codes + fmt.Sprintf(format, values...) + Reset
		logQ <- m
	}
	if fileLog != nil {
		if priority < fileVerbosity {
			fileQ <- fmt.Sprintf(format, values...)
		}
	}
}

//SetLogFile sets the log file info for the log file
// filename is the name of the file (fully qualified path or it will be created in the current directory if possible)
// verbosity is the minimum level of priority needed for a message to be logged
// returns true if the log file was created and opened OK
func SetLogFile(filename string, verbosity int) (bool, error) {
	var err error
	fileLog, err = openLogFile(filename)
	fileVerbosity = verbosity
	return (err == nil), err
}

//CloseLogFile closes any open log file
func CloseLogFile() {
	fileLog = nil
}

//SetVerbosity sets the verbosity level for stdOut: the minimum level of priority that a message needs to have to be output
// e.g. a message of priority 3 will appear on a log of verbosity 5, a message of priority 6 will not
func SetVerbosity(verbosity int) {
	logVerbosity = verbosity
}

func openLogFile(fn string) (*log.Logger, error) {
	fh, err := os.Create(fn)
	if err != nil {
		return nil, err
	}
	l := log.New(fh, "", log.LstdFlags|log.Lmicroseconds)
	return l, nil
}

func watchLog(q chan string) {
	for {
		log.Println(<-q)
	}
}
func watchFile(q chan string) {
	for {
		fileLog.Println(<-q)
	}
}
